package com.paradox.application.domain

import com.paradox.application.database.dao.RecipeDao
import com.paradox.application.entity.Recipe
import com.paradox.application.network.Endpoint
import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Esteban Lopez
 * Paradox
 */

class RecipeInteractorImpl @Inject constructor(
    private val apiRepository: Endpoint,
    private val recipeDao: RecipeDao,
    @Named("ioScheduler") private val ioScheduler: Scheduler
) : RecipeInteractor {

    override fun fetchRecipesAndSave(): Completable {
        return apiRepository.recipes().subscribeOn(ioScheduler).flatMapCompletable {
            recipeDao.insert(it)
        }
    }

    override fun fetchLocalRecipes(): Single<List<Recipe>> {
        return recipeDao.recipes().subscribeOn(ioScheduler)
    }

    override fun fetchLocalRecipesByTitle(title: String?): Single<List<Recipe>> {
        title?.let {
            return recipeDao.recipesByTitle("%$title%").subscribeOn(ioScheduler)
        } ?: run {
            return recipeDao.recipes().subscribeOn(ioScheduler)
        }
    }

    override fun fetchLocalRecipeById(id: Long): Single<Recipe> {
        return apiRepository.recipeById(id).subscribeOn(ioScheduler)
    }
}