package com.paradox.application.domain

import com.paradox.application.entity.Recipe
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Created by Esteban Lopez
 * Paradox
 */

interface RecipeInteractor {
    fun fetchRecipesAndSave() : Completable
    fun fetchLocalRecipes() : Single<List<Recipe>>
    fun fetchLocalRecipesByTitle(title: String?) : Single<List<Recipe>>
    fun fetchLocalRecipeById(id: Long) : Single<Recipe>
}