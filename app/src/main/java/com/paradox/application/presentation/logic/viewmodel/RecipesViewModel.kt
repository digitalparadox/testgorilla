package com.paradox.application.presentation.logic.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.paradox.application.domain.RecipeInteractor
import com.paradox.application.entity.Recipe
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Esteban Lopez
 * Paradox
 */

class RecipesViewModel @Inject constructor(
    private val recipeInteractor: RecipeInteractor,
    @Named("ioScheduler") private val ioScheduler: Scheduler
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val navigationEvents = MutableLiveData<RecipesNavigation>()
    val recipes = MutableLiveData<List<Recipe>>().apply { this.value = emptyList() }
    val recipe = MutableLiveData<Recipe>()
    val isEmpty = ObservableBoolean(true)

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun onInit() {
        compositeDisposable.add(
            recipeInteractor.fetchLocalRecipes()
                .observeOn(ioScheduler)
                .subscribe({
                    isEmpty.set(it.isNullOrEmpty())
                    recipes.postValue(it)
                }, {
                    isEmpty.set(true)
                })
        )
    }

    fun onNavigateToRecipeDetail(id: Long) {
        navigationEvents.postValue(RecipesNavigation.OnRecipeDetail(id = id))
    }

    fun onRecipeById(id: Long) {
        compositeDisposable.add(recipeInteractor.fetchLocalRecipeById(id).observeOn(ioScheduler).subscribe({
            recipe.postValue(it)
        }, {}))
    }

    fun onSearchByTitle(title: String?) {
        compositeDisposable.add(
            recipeInteractor.fetchLocalRecipesByTitle(title)
                .observeOn(ioScheduler)
                .subscribe({
                    isEmpty.set(it.isNullOrEmpty())
                    recipes.postValue(it)
                }, {
                    isEmpty.set(true)
                })
        )
    }

    sealed class RecipesNavigation {
        data class OnRecipeDetail(val id: Long) : RecipesNavigation()
    }
}