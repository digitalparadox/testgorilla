package com.paradox.application.presentation.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.paradox.application.R
import com.paradox.application.BR
import com.paradox.application.databinding.FragmentRecipeDetailBinding
import com.paradox.application.presentation.logic.viewmodel.RecipesViewModel
import com.paradox.application.presentation.logic.viewmodel.ViewModelFactory
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Created by Esteban Lopez
 * Paradox
 */

class RecipeDetailFragment: Fragment() {

    @Inject lateinit var viewModelFactory: ViewModelFactory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[RecipesViewModel::class.java]
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil
            .inflate<FragmentRecipeDetailBinding>(inflater, R.layout.fragment_recipe_detail, container, false).also {
                it.lifecycleOwner = this@RecipeDetailFragment
                it.setVariable(BR.vm, viewModel)
            }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.onRecipeById(arguments!!.getLong(RECIPE_ID))
    }

    companion object {
        fun newInstance(recipeId: Long) = RecipeDetailFragment().apply {
            arguments = Bundle().apply {
                putLong(RECIPE_ID, recipeId)
            }
        }

        const val TAG = "RecipeDetailFragment"
        private const val RECIPE_ID = "RECIPE_ID"
    }
}