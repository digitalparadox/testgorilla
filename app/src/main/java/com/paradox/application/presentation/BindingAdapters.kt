package com.paradox.application.presentation

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.RatingBar
import androidx.databinding.BindingAdapter
import com.paradox.application.GlideApp

/**
 * Created by Esteban Lopez
 * Paradox
 */

@BindingAdapter("isVisible")
fun setIsVisible(view: View, visible: Boolean) {
    if (visible) {
        view.visibility = View.VISIBLE
    } else {
        view.visibility = View.GONE
    }
}

@BindingAdapter("bind:loadImageByUri", "placeholder", requireAll = false)
fun setGlideImageByUri(view: ImageView, url: String?, placeholderDrawable: Drawable?) {
    if (url != null) {
        GlideApp
            .with(view.context)
            .load(url)
            .apply {
                placeholderDrawable?.let { placeholder(it) }
            }
            .centerCrop()
            .into(view)
    }
}

@BindingAdapter("android:rating")
fun setRating(view: RatingBar, rating: Float?) {
    rating?.let {
        view.rating = it
    }
}