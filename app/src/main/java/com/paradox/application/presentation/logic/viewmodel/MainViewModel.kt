package com.paradox.application.presentation.logic.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.paradox.application.domain.RecipeInteractor
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Esteban Lopez
 * Paradox
 */

class MainViewModel @Inject constructor(
    private val recipeInteractor: RecipeInteractor,
    @Named("ioScheduler") private val ioScheduler: Scheduler
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val navigationEvents = MutableLiveData<MainNavigation>()

    fun onInit() {
        compositeDisposable.add(
            recipeInteractor.fetchRecipesAndSave()
                .observeOn(ioScheduler)
                .delay(2, TimeUnit.SECONDS)
                .subscribe({
                    navigationEvents.postValue(MainNavigation.OnFetchRecipesSuccess)
                }, {})
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    sealed class MainNavigation {
        object OnFetchRecipesSuccess : MainNavigation()
    }
}