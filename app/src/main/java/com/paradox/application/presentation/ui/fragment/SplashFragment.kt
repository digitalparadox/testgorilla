package com.paradox.application.presentation.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.paradox.application.R
import com.paradox.application.databinding.FragmentSplashBinding
import dagger.android.support.AndroidSupportInjection

/**
 * Created by Esteban Lopez
 * Paradox
 */

class SplashFragment: Fragment() {

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
       return DataBindingUtil.inflate<FragmentSplashBinding>(inflater, R.layout.fragment_splash, container, false).root
    }
}