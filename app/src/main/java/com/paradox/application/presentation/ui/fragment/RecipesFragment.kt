package com.paradox.application.presentation.ui.fragment

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView.OnQueryTextListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.paradox.application.R
import com.paradox.application.BR
import com.paradox.application.databinding.FragmentRecipesBinding
import com.paradox.application.entity.Recipe
import com.paradox.application.presentation.logic.viewmodel.RecipesViewModel
import com.paradox.application.presentation.logic.viewmodel.ViewModelFactory
import com.paradox.application.presentation.ui.adapter.BoundRecyclerAdapter
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Created by Esteban Lopez
 * Paradox
 */

class RecipesFragment: Fragment() {

    @Inject lateinit var viewModelFactory: ViewModelFactory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[RecipesViewModel::class.java]
    }

    private lateinit var binding: FragmentRecipesBinding
    private lateinit var boundAdapter: BoundRecyclerAdapter<Recipe>

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil
            .inflate<FragmentRecipesBinding>(inflater, R.layout.fragment_recipes, container, false).also {
                it.lifecycleOwner = this@RecipesFragment
                it.setVariable(BR.vm, viewModel)
            }

        boundAdapter = BoundRecyclerAdapter(viewModel) { R.layout.item_recipe }

        binding.recyclerProducts.apply {
            adapter = boundAdapter
            layoutManager = LinearLayoutManager(binding.recyclerProducts.context, RecyclerView.VERTICAL, false)
        }

        observeRecipes()
        observeNavigations()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSearchView()
    }

    private fun setupSearchView() {
        binding.searchView.setOnQueryTextListener(object : OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (TextUtils.isEmpty(newText)) {
                    viewModel.onSearchByTitle(title = null)
                    binding.root.requestFocus()
                } else {
                    viewModel.onSearchByTitle(title = newText)
                }

                return false
            }
        })
    }

    private fun observeRecipes() {
        viewModel.recipes.observe(viewLifecycleOwner, Observer {
            boundAdapter.data = it
        })

        viewModel.onInit()
    }

    private fun observeNavigations() {
        viewModel.navigationEvents.observe(viewLifecycleOwner, Observer {
            when (it) {
                is RecipesViewModel.RecipesNavigation.OnRecipeDetail -> {
                    activity!!.supportFragmentManager.beginTransaction()
                        .add(R.id.fragment_nav_host, RecipeDetailFragment.newInstance(it.id))
                        .addToBackStack(RecipeDetailFragment.TAG)
                        .commit()
                }
            }
        })
    }
}