package com.paradox.application.presentation.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.paradox.application.R
import com.paradox.application.databinding.ActivityMainBinding
import com.paradox.application.presentation.logic.viewmodel.MainViewModel
import com.paradox.application.presentation.logic.viewmodel.ViewModelFactory
import com.paradox.application.presentation.ui.fragment.RecipesFragment
import com.paradox.application.presentation.ui.fragment.SplashFragment
import dagger.android.AndroidInjection
import javax.inject.Inject

/**
 * Created by Esteban Lopez
 * Paradox
 */

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main).apply {
            lifecycleOwner = this@MainActivity
        }

        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_nav_host, SplashFragment())
            .commit()

        viewModel.navigationEvents.observe(this, Observer {
            when (it) {
                MainViewModel.MainNavigation.OnFetchRecipesSuccess -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_nav_host, RecipesFragment())
                        .commit()
                }
            }
        })

        viewModel.onInit()
    }
}