package com.paradox.application.network

import com.paradox.application.entity.Recipe
import io.reactivex.Single
import retrofit2.http.*

/**
 * Created by Esteban Lopez
 * Paradox
 */

interface Endpoint {

    @Headers("Content-Type: application/json")
    @GET("/recipes")
    fun recipes(): Single<List<Recipe>>

    @Headers("Content-Type: application/json")
    @GET("/recipes/{id}")
    fun recipeById(@Path("id") id: Long): Single<Recipe>
}
