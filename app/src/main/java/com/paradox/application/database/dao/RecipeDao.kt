package com.paradox.application.database.dao

import androidx.room.*
import com.paradox.application.entity.Recipe
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Created by Esteban Lopez
 * Paradox
 */

@Dao
abstract class RecipeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(recipe: List<Recipe>) : Completable

    @Query("DELETE FROM Recipe")
    abstract fun deleteRecipes()

    @Query("SELECT * FROM Recipe")
    abstract fun recipes() : Single<List<Recipe>>

    @Query("SELECT * FROM Recipe WHERE title LIKE :value")
    abstract fun recipesByTitle(value: String) : Single<List<Recipe>>

    @Query("SELECT * FROM Recipe WHERE id = :id")
    abstract fun recipeById(id: Int) : Single<Recipe>
}