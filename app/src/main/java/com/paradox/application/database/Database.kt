package com.paradox.application.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.paradox.application.database.dao.*
import com.paradox.application.entity.*

/**
 * Created by Esteban Lopez
 * Paradox
 */

@Database(
    version = 1,
    entities = [
        Recipe::class
    ], exportSchema = false)
abstract class Database: RoomDatabase() {
    abstract fun recipeDao() : RecipeDao
}
