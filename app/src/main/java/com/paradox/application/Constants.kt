package com.paradox.application

/**
 * Created by Esteban Lopez
 * Paradox
 */

class Constants {
    companion object {
        const val BASE_URL = "https://gl-endpoint.herokuapp.com/"
    }
}