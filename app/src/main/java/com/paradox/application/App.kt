package com.paradox.application

import android.app.Activity
import android.app.Application
import androidx.fragment.app.Fragment
import com.paradox.application.di.component.ApplicationComponent
import com.paradox.application.di.component.DaggerApplicationComponent
import com.paradox.application.di.module.AppModule
import com.paradox.application.di.module.NetworkModule
import com.paradox.application.di.module.RxModule
import com.paradox.application.network.Endpoint
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by Esteban Lopez
 * Paradox
 */

class App: Application(), HasActivityInjector, HasSupportFragmentInjector {

    private lateinit var component: ApplicationComponent
    private var apiRepository: Endpoint? = null

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var dispatchingFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        component = DaggerApplicationComponent
            .builder()
            .appModule(AppModule(this))
            .networkModule(NetworkModule(apiRepository))
            .rxModule(RxModule())
            .build()

        component.injectMembers(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingActivityInjector
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return dispatchingFragmentInjector
    }
}