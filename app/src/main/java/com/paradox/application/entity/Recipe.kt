package com.paradox.application.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Esteban Lopez
 * Paradox
 */

@Entity
data class Recipe(
    @PrimaryKey
    val id: Long,
    val title: String,
    val rating: Float?,
    val image: String?,
    val instructions: String?
)