package com.paradox.application

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by Esteban Lopez
 * Paradox
 */

@GlideModule
class GlideAppModule : AppGlideModule()