package com.paradox.application.di.module

import com.google.gson.*
import com.paradox.application.Constants
import com.paradox.application.network.Endpoint
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Esteban Lopez
 * Paradox
 */

@Module
class NetworkModule(private val apiRepository: Endpoint?) {

    @Provides
    fun providesWalletRepository(): Endpoint {

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()

        val gson = GsonBuilder().create()

        return apiRepository ?: Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(client)
            .build().create(Endpoint::class.java)
    }

    @Provides
    fun providesGSON() = Gson()
}