package com.paradox.application.di.module

import android.content.Context
import androidx.room.Room
import com.paradox.application.database.Database
import com.paradox.application.database.dao.*
import dagger.Module
import dagger.Provides

/**
 * Created by Esteban Lopez
 * Paradox
 */

@Module
internal class DatabaseModule(
    private var recipeDao: RecipeDao? = null
) {
    @Provides
    fun providesAuthDatabase(context: Context): Database {
        return Room.databaseBuilder(context, Database::class.java, "db").build()
    }

    @Provides
    fun providesRecipeDao(database: Database) = recipeDao ?: database.recipeDao().also { recipeDao = it }
}