package com.paradox.application.di.component

import com.paradox.application.App
import com.paradox.application.di.module.*
import com.paradox.application.di.module.DatabaseModule
import com.paradox.application.di.module.ViewBindingModule
import dagger.Component
import dagger.MembersInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by Esteban Lopez
 * Paradox
 */

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    DatabaseModule::class,
    NetworkModule::class,
    ViewBindingModule::class,
    ViewModelModule::class,
    RxModule::class,
    InteractorModule::class
])
interface ApplicationComponent: MembersInjector<App>