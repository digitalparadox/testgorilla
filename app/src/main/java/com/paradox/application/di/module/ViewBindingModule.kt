package com.paradox.application.di.module

import com.paradox.application.presentation.ui.MainActivity
import com.paradox.application.presentation.ui.fragment.RecipeDetailFragment
import com.paradox.application.presentation.ui.fragment.RecipesFragment
import com.paradox.application.presentation.ui.fragment.SplashFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Esteban Lopez
 * Paradox
 */

@Module
internal abstract class ViewBindingModule {

    @ContributesAndroidInjector
    abstract fun providesMainActivity() : MainActivity

    @ContributesAndroidInjector
    abstract fun providesRecipesFragment() : RecipesFragment

    @ContributesAndroidInjector
    abstract fun providesRecipeDetailFragment() : RecipeDetailFragment

    @ContributesAndroidInjector
    abstract fun providesSplashFragment() : SplashFragment
}