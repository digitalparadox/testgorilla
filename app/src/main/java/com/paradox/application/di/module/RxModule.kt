package com.paradox.application.di.module

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named

/**
 * Created by Esteban Lopez
 * Paradox
 */

@Module
internal class RxModule {

    @Provides
    @Named("mainScheduler")
    fun provideMainScheduler() : Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Provides
    @Named("ioScheduler")
    fun provideIOScheduler() : Scheduler {
        return Schedulers.io()
    }
}