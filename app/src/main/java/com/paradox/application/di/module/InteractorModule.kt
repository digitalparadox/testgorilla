package com.paradox.application.di.module

import com.paradox.application.domain.RecipeInteractor
import com.paradox.application.domain.RecipeInteractorImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Created by Esteban Lopez
 * Paradox
 */

@Module
abstract class InteractorModule {
    @Binds
    @Singleton
    abstract fun bindAppInteractor(recipeInteractor: RecipeInteractorImpl) : RecipeInteractor
}