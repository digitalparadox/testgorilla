package com.paradox.application.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * Created by Esteban Lopez
 * Paradox
 */

@Module
internal class AppModule constructor(private val application: Application) {

    @Provides
    fun providesContext(): Context = application
}