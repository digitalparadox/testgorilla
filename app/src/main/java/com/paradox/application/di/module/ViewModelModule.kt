package com.paradox.application.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.paradox.application.presentation.logic.viewmodel.MainViewModel
import com.paradox.application.presentation.logic.viewmodel.RecipesViewModel
import com.paradox.application.presentation.logic.viewmodel.ViewModelFactory
import com.paradox.application.presentation.logic.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Esteban Lopez
 * Paradox
 */

@Module
internal abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModelModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RecipesViewModel::class)
    abstract fun bindRecipesViewModel(recipesViewModel: RecipesViewModel): ViewModel
}
